package mx.tec.sep30;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements DoggoFragment.Callback, Handler.Callback {

    private static final String FRAGMENT_TAG = "fragmentito";
    private DoggoFragment doggo;
    private CatFragment catto;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        doggo = DoggoFragment.newInstance("fido", "10 años");
        doggo.desdeAfuera();

        catto = new CatFragment();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.contenedor, doggo, FRAGMENT_TAG);
        // transaction.add(R.id.contenedor, catto, "GATO");
        transaction.commit();

        // get handler so we can actually exchange messages
        handler = new Handler(Looper.getMainLooper(), this);
    }

    @Override
    public void hacerCallback(String mensaje) {

        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }


    private void swap(Fragment newFragment){

        // obtener referencia al fragmento que ocupa el espacio actual
        FragmentManager manager = getSupportFragmentManager();
        Fragment f = manager.findFragmentByTag(FRAGMENT_TAG);


        if(f != null && f != newFragment){

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.remove(f);
            transaction.add(R.id.contenedor, newFragment, FRAGMENT_TAG);
            transaction.commit();
        }
    }

    public void showDoggo(View v){

        swap(doggo);
    }

    public void showCatto(View v){

        swap(catto);
    }

    public void doRequest(View v) {

        Request request = new Request("https://api.github.com/users", handler);
        request.start();
    }

    @Override
    public boolean handleMessage(@NonNull Message message) {

        JSONArray data = (JSONArray) message.obj;
        Toast.makeText(this, data.toString(), Toast.LENGTH_SHORT).show();


        try {
            // JSON
            // javascript object notation
            String ejemplo1 = "{'nombre' : 'Diego', 'calificacion' : 12, 'aprobado' : false}";

            JSONObject ejemplo1JSON = new JSONObject(ejemplo1);
            Log.wtf("JSON", ejemplo1JSON.getString("nombre"));
            Log.wtf("JSON", ejemplo1JSON.getInt("calificacion") + "");
            Log.wtf("JSON", ejemplo1JSON.getBoolean("aprobado") + "");

            String ejemplo2 = "[{'nombre' : 'Diego', 'calificacion' : 12, 'aprobado' : false}," +
                    "{'nombre' : 'Luis Angel', 'calificacion' : 9, 'aprobado' : false}," +
                    "{'nombre' : 'Hugo', 'calificacion' : 6, 'aprobado' : false}]";

            JSONArray ejemplo2JSON = new JSONArray(ejemplo2);

            for(int i = 0; i < ejemplo2JSON.length(); i++){

                JSONObject actual = ejemplo2JSON.getJSONObject(i);
                Log.wtf("JSON 2", actual.getString("nombre"));
                Log.wtf("JSON 2", actual.getInt("calificacion") + "");
                Log.wtf("JSON 2", actual.getBoolean("aprobado") + "");
            }

            String ejemplo3 = "{'nombre' : 'Santiago', 'calificacion' : 12, 'aprobado' : false, 'computadora': {'modelo' : 'macbook'}}";
            JSONObject ejemplo3JSON = new JSONObject(ejemplo3);
            JSONObject computadora = ejemplo3JSON.getJSONObject("computadora");
            Log.wtf("JSON 3", computadora.getString("modelo"));


            String ejemplo4 = "{'nombre' : 'Oscar', 'calificacion' : 12, 'aprobado' : false, 'parciales' : [20, 15, 12]}";
            JSONObject ejemplo4JSON = new JSONObject(ejemplo4);
            JSONArray parciales = ejemplo4JSON.getJSONArray("parciales");

            for(int i = 0; i < parciales.length(); i++){

                Log.wtf("JSON 4", parciales.getDouble(i) + "");
            }

            for(int i = 0; i < data.length(); i++){

                JSONObject actual = data.getJSONObject(i);
                Log.wtf("JSON GITHUB", actual.getString("login"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }
}