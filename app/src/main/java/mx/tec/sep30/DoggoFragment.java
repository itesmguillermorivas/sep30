package mx.tec.sep30;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DoggoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DoggoFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String NOMBRE = "nombre";
    private static final String EDAD = "edad";

    private String nombre;
    private String edad;

    private Context context;
    private Callback listener;

    public DoggoFragment() {
        // Required empty public constructor
    }

    public static DoggoFragment newInstance(String nombre, String edad) {
        DoggoFragment fragment = new DoggoFragment();
        Bundle args = new Bundle();
        args.putString(NOMBRE, nombre);
        args.putString(EDAD, edad);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            nombre = getArguments().getString(NOMBRE);
            edad = getArguments().getString(EDAD);

            Log.wtf("ON CREATE FRAGMENT", nombre);
            Log.wtf("ON CREATE FRAGMENT", edad);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_doggo, container, false);

        TextView n = v.findViewById(R.id.textView2);
        TextView e = v.findViewById(R.id.textView3);

        n.setText(nombre);
        e.setText(edad);

        Button toastito = v.findViewById(R.id.button3);
        Button callbackcito = v.findViewById(R.id.button4);

        toastito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "PRUEBA", Toast.LENGTH_SHORT).show();
            }
        });

        callbackcito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.hacerCallback("SALUDOS DEL DOGGO!");
            }
        });

        return v;
    }


    public void desdeAfuera() {

        Log.wtf("DESDE AFUERA", "ESTO SE INVOCO DESDE FUERA");
    }

    public void onAttach(Context context){
        super.onAttach(context);
        this.context = context;

        if(context instanceof Callback){

            listener = (Callback)context;
        } else {

            throw new RuntimeException("Callback no implementado");
        }
    }

    public interface Callback{

        public void hacerCallback(String mensaje);
    }
}